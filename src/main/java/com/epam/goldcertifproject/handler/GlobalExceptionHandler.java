package com.epam.goldcertifproject.handler;


import com.epam.goldcertifproject.dto.ErrorResponse;
import com.epam.goldcertifproject.exception.BatchException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse response(MethodArgumentNotValidException e, WebRequest webRequest){
        printToLog(e);
        List<String> errorList = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorList.add(objectError.toString()));
        return new ErrorResponse(new Date().toString(), errorList.toString(), webRequest.getDescription(false));
    }


    @ExceptionHandler(BatchException.class)
    @ResponseStatus(HttpStatus.OK)
    ErrorResponse response( BatchException batchException, WebRequest webRequest){
        printToLog(batchException);
        return new ErrorResponse(new Date().toString(), batchException.getMessage(), webRequest.getDescription(false));
    }


    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorResponse response(RuntimeException runtimeException, WebRequest webRequest){
        printToLog(runtimeException);
        return new ErrorResponse(new Date().toString(), runtimeException.getMessage(), webRequest.getDescription(false));
    }

    private void printToLog(Exception e) {
        log.info(ExceptionUtils.getStackTrace(e));
    }


}
