package com.epam.goldcertifproject.util;

public enum StringConstants {
    ENTERED_CONTROLLER("Entered {} controller of {} class with value {}"),
    EXITING_CONTROLLER("Exiting {} controller of {} class"),
    ENTERED_SERVICE_METHOD("Entered {} service of {} class with value {}"),
    EXITING_SERVICE_METHOD("Exiting {} service of {} class"),
    ERROR_MESSAGE("Exception risen in {} method of {} class, Exiting the entered method");
    private final String value;
    StringConstants(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
