package com.epam.goldcertifproject.util;

import com.epam.goldcertifproject.dto.AssociateDTO;
import com.epam.goldcertifproject.dto.BatchDTO;
import com.epam.goldcertifproject.model.Associate;
import com.epam.goldcertifproject.model.Batch;

import java.util.ArrayList;
import java.util.List;

public class ValueMapper {
    public static Batch createBatchFromDTO(BatchDTO batchDTO){
        Batch batch = new Batch();
        batch.setName(batchDTO.getBatchName());
        List<Associate> associateList = new ArrayList<>();
        batchDTO.getAssociateDTOList().forEach( associateDTO -> {
            Associate associate = new Associate();
            if(associateDTO.getId() != 0)
                associate.setId(associateDTO.getId());
            associate.setName(associateDTO.getName());
            associate.setEmail(associateDTO.getEmail());
            associate.setBatch(batch);
            associateList.add(associate);
        });
        batch.setAssociateList(associateList);
        return batch;
    }
    public static BatchDTO createBatchDTOFromModel(Batch batch){
        BatchDTO batchDTO = new BatchDTO();
        batchDTO.setId(batch.getId());
        batchDTO.setBatchName(batch.getName());
        List<AssociateDTO> associateDTOList = new ArrayList<>();
        batch.getAssociateList().forEach(associate -> {
            AssociateDTO associateDTO = new AssociateDTO();
            associateDTO.setId(associate.getId());
            associateDTO.setName(associate.getName());
            associateDTO.setEmail(associate.getEmail());
            associateDTOList.add(associateDTO);
        });
        batchDTO.setAssociateDTOList(associateDTOList);
        return batchDTO;
    }
}
