package com.epam.goldcertifproject.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class BatchDTO {
    private int id;
    @NotBlank(message = "Give batch name")
    private String batchName;
    @NotEmpty(message = "Give atleast one Value")
    private List<AssociateDTO> associateDTOList;
}
