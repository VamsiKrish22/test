package com.epam.goldcertifproject.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorResponse {
    String time;
    String errorMessage;
    String path;
}
