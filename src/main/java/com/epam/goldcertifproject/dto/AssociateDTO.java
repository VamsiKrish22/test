package com.epam.goldcertifproject.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AssociateDTO {
    private int id;
    @NotBlank(message = "give name")
    private String name;
    @NotBlank(message = "give valid email")
    private String email;
}
