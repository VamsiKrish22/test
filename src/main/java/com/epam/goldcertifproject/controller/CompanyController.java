package com.epam.goldcertifproject.controller;

import com.epam.goldcertifproject.dto.BatchDTO;
import com.epam.goldcertifproject.exception.BatchException;
import com.epam.goldcertifproject.model.ApiResponse;
import com.epam.goldcertifproject.service.BatchServiceImpl;
import com.epam.goldcertifproject.util.StringConstants;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Slf4j
@RestController
@RequestMapping("/company")
@RequiredArgsConstructor
public class CompanyController {

    public static final String ADD_NEW_BATCH = "addNewBatch";
    public static final String FETCH_BATCH = "fetchBatch";
    public static final String UPDATE_BATCH = "updateBatch";
    public static final String DELETE_BATCH = "deleteBatch";
    private final BatchServiceImpl batchService;
    @PostMapping("/addNewBatch")
    ResponseEntity<ApiResponse<BatchDTO>> addNewBatch(@Valid @RequestBody BatchDTO batchDTO){
        log.info(StringConstants.ENTERED_CONTROLLER.getValue(), ADD_NEW_BATCH, this.getClass().getName(), batchDTO.toString());
        batchDTO = batchService.add(batchDTO);
        ApiResponse<BatchDTO> apiResponse = new ApiResponse<>();
        apiResponse.setResult(batchDTO);
        log.info(StringConstants.EXITING_CONTROLLER.getValue(), ADD_NEW_BATCH);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
    @GetMapping("/{batchId}")
    ResponseEntity<ApiResponse<BatchDTO>> fetchBatch(@Valid @NotNull @PathVariable int batchId) throws BatchException {
        log.info(StringConstants.ENTERED_CONTROLLER.getValue(), FETCH_BATCH, this.getClass().getName(), batchId);
        BatchDTO batchDTO = batchService.get(batchId);
        ApiResponse<BatchDTO> apiResponse = new ApiResponse<>();
        apiResponse.setResult(batchDTO);
        log.info(StringConstants.EXITING_CONTROLLER.getValue(), FETCH_BATCH);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
    @PutMapping("/updateBatch/{batchId}")
    ResponseEntity<BatchDTO> updateExistingBatch(@Valid @NotNull @PathVariable int batchId, @Valid @RequestBody BatchDTO batchDTO){
        log.info(StringConstants.ENTERED_CONTROLLER.getValue(), UPDATE_BATCH, this.getClass().getName(),batchId+"  "+ batchDTO.toString());
        batchDTO = batchService.update(batchId, batchDTO);
        log.info(StringConstants.EXITING_CONTROLLER.getValue(), UPDATE_BATCH);
        return new ResponseEntity<>(batchDTO, HttpStatus.OK);
    }
    @DeleteMapping("/{batchId}")
    ResponseEntity<BatchDTO> deleteBatch(@Valid @NotNull @PathVariable int batchId){
        log.info(StringConstants.ENTERED_CONTROLLER.getValue(), DELETE_BATCH, this.getClass().getName(),batchId);
        batchService.remove(batchId);
        log.info(StringConstants.EXITING_CONTROLLER.getValue(), DELETE_BATCH);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
