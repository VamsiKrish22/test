package com.epam.goldcertifproject.repository;

import com.epam.goldcertifproject.model.Associate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, Integer> {
}
