package com.epam.goldcertifproject.exception;


public class BatchException extends Exception {
    public BatchException(String message) {
        super(message);
    }
}
