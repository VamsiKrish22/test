package com.epam.goldcertifproject;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class GoldCertifProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoldCertifProjectApplication.class, args);
	}

}
