package com.epam.goldcertifproject.service;

import com.epam.goldcertifproject.dto.BatchDTO;
import com.epam.goldcertifproject.exception.BatchException;
import com.epam.goldcertifproject.model.Batch;
import com.epam.goldcertifproject.repository.BatchRepository;
import com.epam.goldcertifproject.util.ValueMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BatchServiceImpl {
    private final BatchRepository batchRepository;

    public BatchDTO add(BatchDTO batchDTO){
        Batch batch = ValueMapper.createBatchFromDTO(batchDTO);
        batch = batchRepository.save(batch);
        batchDTO = ValueMapper.createBatchDTOFromModel(batch);
        return batchDTO;
    }

    public BatchDTO update(int batchId, BatchDTO batchDTO){
        Batch batch = ValueMapper.createBatchFromDTO(batchDTO);
        batch.setId(batchId);
        batch = batchRepository.save(batch);
        batchDTO = ValueMapper.createBatchDTOFromModel(batch);
        return batchDTO;
    }

    public BatchDTO get(int batchId) throws BatchException {
        Batch batch = batchRepository.findById(batchId).orElseThrow(() -> new BatchException("Not Found"));
        return ValueMapper.createBatchDTOFromModel(batch);
    }

    public void remove(int batchId){
        batchRepository.deleteById(batchId);
    }

}
