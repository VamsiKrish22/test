package com.epam.goldcertifproject.service;

import com.epam.goldcertifproject.dto.AssociateDTO;
import com.epam.goldcertifproject.dto.BatchDTO;
import com.epam.goldcertifproject.exception.BatchException;
import com.epam.goldcertifproject.model.Associate;
import com.epam.goldcertifproject.model.Batch;
import com.epam.goldcertifproject.repository.BatchRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;


@ExtendWith(MockitoExtension.class)
public class BatchServiceTest {
    @Mock
    BatchRepository batchRepository;

    @InjectMocks
    BatchServiceImpl batchService;

    Associate associate;
    BatchDTO batchDTO;
    AssociateDTO associateDTO;
    Batch batch;
    @BeforeEach
    void createVals(){
        associateDTO = new AssociateDTO();
        associateDTO.setId(1);
        associateDTO.setName("Test Associate");
        associateDTO.setEmail("test@testMail.com");

        batchDTO = new BatchDTO();
        batchDTO.setBatchName("Test");
        batchDTO.setId(1);
        batchDTO.setAssociateDTOList(Collections.singletonList(associateDTO));

        associate = new Associate();
        associate.setId(1);
        associate.setName("Test Associate");
        associate.setEmail("test@testMail.com");

        batch = new Batch();
        batch.setAssociateList(Collections.singletonList(associate));
        batch.setId(1);
        batch.setName("Test");
        associate.setBatch(batch);
    }

    @Test
    void testAddNewTest(){
        Mockito.when(batchRepository.save(any(Batch.class))).thenReturn(batch);
        assertNotNull(batchService.add(batchDTO));
        Mockito.verify(batchRepository).save(any(Batch.class));
    }

    @Test
    void testUpdateBatch(){
        Mockito.when(batchRepository.save(any(Batch.class))).thenReturn(batch);
        assertNotNull(batchService.update(1, batchDTO));
        Mockito.verify(batchRepository).save(any(Batch.class));
    }

    @Test
    void testGetBatch() throws BatchException {
        Mockito.when(batchRepository.findById(any(Integer.class))).thenReturn(Optional.of(batch));
        assertNotNull(batchService.get(1));
        Mockito.verify(batchRepository).findById(any(Integer.class));
    }

    @Test
    void testGetBatchExceptionCase(){
        Mockito.when(batchRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(BatchException.class ,() -> batchService.get(1));
        Mockito.verify(batchRepository).findById(any(Integer.class));
    }


    @Test
    void testDelete(){
        Mockito.doNothing().when(batchRepository).deleteById(any(Integer.class));
        assertDoesNotThrow(() -> batchService.remove(1));
        Mockito.verify(batchRepository).deleteById(any(Integer.class));
    }
}
