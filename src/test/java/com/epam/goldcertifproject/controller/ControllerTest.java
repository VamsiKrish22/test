package com.epam.goldcertifproject.controller;


import com.epam.goldcertifproject.dto.AssociateDTO;
import com.epam.goldcertifproject.dto.BatchDTO;
import com.epam.goldcertifproject.exception.BatchException;
import com.epam.goldcertifproject.model.Associate;
import com.epam.goldcertifproject.model.Batch;
import com.epam.goldcertifproject.service.BatchServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BatchServiceImpl batchService;

    Associate associate;
    BatchDTO batchDTO;
    AssociateDTO associateDTO;
    Batch batch;
    @BeforeEach
    void createVals(){
        associateDTO = new AssociateDTO();
        associateDTO.setId(0);
        associateDTO.setName("Test Associate");
        associateDTO.setEmail("test@testMail.com");

        batchDTO = new BatchDTO();
        batchDTO.setBatchName("Test");
        batchDTO.setId(0);
        batchDTO.setAssociateDTOList(Collections.singletonList(associateDTO));

        associate = new Associate();
        associate.setId(0);
        associate.setName("Test Associate");
        associate.setEmail("test@testMail.com");

        batch = new Batch();
        batch.setAssociateList(Collections.singletonList(associate));
        batch.setId(0);
        batch.setName("Test");
    }

    @Test
    void testAddNewBatch() throws Exception {
        Mockito.when(batchService.add(any(BatchDTO.class))).thenReturn(batchDTO);
        mockMvc.perform(post("/company/addNewBatch")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(batchDTO)))
                .andExpect(status().isOk());
        Mockito.verify(batchService).add(any(BatchDTO.class));
    }


    @Test
    void testFetchBatch() throws Exception {
        Mockito.when(batchService.get(any(Integer.class))).thenReturn(batchDTO);
        mockMvc.perform(get("/company/1"))
                .andExpect(status().isOk());
        Mockito.verify(batchService).get(any(Integer.class));
    }

    @Test
    void testUpdateBatch() throws Exception {
        Mockito.when(batchService.update(any(Integer.class), any(BatchDTO.class))).thenReturn(batchDTO);

        mockMvc.perform(put("/company/updateBatch/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(batchDTO)))
                .andExpect(status().isOk());
        Mockito.verify(batchService).update(any(Integer.class), any(BatchDTO.class));
    }

    @Test
    void testDeleteBatch() throws Exception{
        Mockito.doNothing().when(batchService).remove(any(Integer.class));
        mockMvc.perform(delete("/company/1"))
                .andExpect(status().isNoContent());
        Mockito.verify(batchService).remove(any(Integer.class));
    }
}
